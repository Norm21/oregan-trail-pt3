class Traveler {
    constructor(name){
    this.name = name;
    this.food = 1;
    this.isHealthy = true;
}
}
class Wagon{
    constructor (capacity){
    this.capacity = capacity;
    this.passenger = [];
}
}

Traveler.prototype.hunt = function() {
    this.food = this.food + 2;
}

Traveler.prototype.eat = function(){
    if(this.food === 0){
        this.isHealthy = false
        return
    }
    this.food = this.food -1;
}
class Doctor extends Traveler {
    constructor(name){
        super(name)
    }
    
}  

class Hunter extends Traveler{
    constructor(name){
        super(name)
        this.food = 2;
    }
    
}

Doctor.prototype = Object.create(Traveler.prototype);
Doctor.prototype.contructor = Doctor;
Doctor.prototype.heal = function(traveler){
    traveler.isHealthy = true
}

Hunter.prototype = Object.create(Traveler.prototype);
Hunter.prototype.contructor = Hunter;

Hunter.prototype.hunt = function(){
    this.food = this.food + 5;
}

Hunter.prototype.eat = function(){
    if(this.food < 2){
        this.isHealthy = false 
        this.food = 0
    }else{
      this.food = this.food -2;  
    }
}

Hunter.prototype.giveFood = function(traveler, numOfFoodUnits){
    if(this.food < numOfFoodUnits){
        return 
    }
    this.food = this.food - numOfFoodUnits
    traveler.food = traveler.food + numOfFoodUnits
}


Wagon.prototype.shouldQuarantine = function(){
    let isSick
    for(person of this.passenger){
        if(person.isHealthy)
        {isSick = false}
        else{return true}
    }
    return isSick
}
Wagon.prototype.totalFood = function(){
    let totalFood = 0
    for (let i = 0; i < this.passenger.length; i++) {
        totalFood += this.passenger[i].food
        
    }
    return totalFood
}
Wagon.prototype.getAvailableSeatCount = function(){
    return this.capacity - this.passenger.length
}
Wagon.prototype.join = function(newPassenger){
    if(this.getAvailableSeatCount()){
        this.passenger.push(newPassenger)
    }
    

}

// Create a wagon that can hold 4 people
let wagon = new Wagon(4);
// Create five travelers
let henrietta = new Traveler('Henrietta');
let juan = new Traveler('Juan');
let drsmith = new Doctor('Dr. Smith');
let sarahunter = new Hunter('Sara');
let maude = new Traveler('Maude');
console.log(`#1: There should be 4 available seats. Actual: ${wagon.getAvailableSeatCount()}`);
wagon.join(henrietta);
console.log(`#2: There should be 3 available seats. Actual: ${wagon.getAvailableSeatCount()}`);
wagon.join(juan);
wagon.join(drsmith);
wagon.join(sarahunter);
wagon.join(maude); // There isn't room for her!
console.log(`#3: There should be 0 available seats. Actual: ${wagon.getAvailableSeatCount()}`);
console.log(`#4: There should be 5 total food. Actual: ${wagon.totalFood()}`);
sarahunter.hunt(); // gets 5 more food
drsmith.hunt();
console.log(`#5: There should be 12 total food. Actual: ${wagon.totalFood()}`);
henrietta.eat();
sarahunter.eat();
drsmith.eat();
juan.eat();
juan.eat(); // juan is now hungry (sick)
console.log(`#6: Quarantine should be true. Actual: ${wagon.shouldQuarantine()}`);
console.log(`#7: There should be 7 total food. Actual: ${wagon.totalFood()}`);
drsmith.heal(juan);
console.log(`#8: Quarantine should be false. Actual: ${wagon.shouldQuarantine()}`);
sarahunter.giveFood(juan, 4);
sarahunter.eat(); // She only has 1, so she eats it and is now sick
console.log(`#9: Quarantine should be true. Actual: ${wagon.shouldQuarantine()}`);
console.log(`#10: There should be 6 total food. Actual: ${wagon.totalFood()}`);